<?php
/**********************************************************************************
 *
 * @copyright copyright (c) 2012 Matt Chesstale 
 * @license GNU General Public License version 3
 * pipeline "post_propre" happens after the HTML iframe generation by pgn4spip_fonctions.php
 *
 **********************************************************************************/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function pgn4spip_postpropre($flux)	{

	if (strpos($flux, CLS_CHESSS))
	{
		$regex = "@<div.*" . CLS_CHESSS . ".*</div>@msU";
		preg_match_all($regex, $flux, $matches, PREG_PATTERN_ORDER); // find all instances of generated plugin
		$countPlugins = count($matches[0]); // Number of plugins
		if ($countPlugins > 0) // iframe(s) found
		{
			for ( $indPlugin=0; $indPlugin < $countPlugins; $indPlugin++ ) // For each iframe
			{	
				$strHtml = $matches[indFullPattern][$indPlugin];
				$strHtml = str_replace("&#123;", "{", $strHtml); // Replace "&#123;PGN comment&#125;" with 
				$strHtml = str_replace("&#125;", "}", $strHtml); // "{PGN comment}"
				$flux = str_replace($matches[indFullPattern][$indPlugin], $strHtml, $flux);
			}	
		}		
	}
	return $flux;
}