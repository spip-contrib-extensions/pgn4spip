<?php
/**********************************************************************************
 * @Subject French resources: this is a SPIP language file
 * @package pgn4spip plugin to embed pgn4web chessboard in a SPIP 2.x or 3.x article
 * @copyright copyright (c) 2012 Matt Chesstale 
 * @license GNU General Public License version 3
 **********************************************************************************/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

// Only lowercase in the key. 
'ok'				=>	'OK',
'reinit'			=>	'Remettre les valeurs par défaut',
'config_reinit'		=>	'La configuration a été remise avec les valeurs par défaut',
'boxtitle'			=>	'Configuration générale de l’échiquier et du PGN',
'squareclick'		=>	'Chaque case de l’échiquier a une infobulle et une fonction.<br />
Pour inverser l’échiquier, cliquer sur la case <b>e7</b> ou appuyer sur touche "<b>f</b>".',
'module'			=>	'Module',
'horizontal'		=>	'horizontal',
'vertical'			=>	'vertical',
'auto'				=>	'Hauteur automatique',
'manual'			=>	'manuelle : ',
'pixels'			=>	' pixels',
'chessboard'		=>	'Echiquier',
'square'			=>	'Case : ',
'white'				=>	'blanche',
'black'				=>	'noire',
'piece'				=>	'Taille pièce :',
'default'			=>	'auto',
'font'				=>	'Police :',
'alpha'				=>	'alpha',
'merida'			=>	'merida',
'uscf'				=>	'uscf',
'svgchess'			=>	'svg',
'focus'				=>	'Focus',
'border'			=>	'Bord',
'background'		=>	'Fond',
'pgn'				=>	'PGN',
'pgnheader'			=>	'Entête',
'pgnmove'			=>	'Coup',
'pgncomment'		=>	'Commentaire',
'pgnfocus'			=>	'Focus',
'pgndelay'			=>	'Délai (ms)',
'showmoves'			=>	'Notation',
'figurine'			=>	'figurine',
'text'				=>	'texte',
'puzzle'			=>	'puzzle',
'hidden'			=>	'cachée',
'live'				=>	'live',
'ctrlbrowse'		=>	'Bouton',
'ctrlarrow'			=>	'Flèche',
'ctrlbckgrnd'		=>	'Fond',
'custom'			=>	'plat',
'standard'			=>	'3D',
'newline'			=>	'bloc',
'inline'			=>	'suite',
'hidden2'			=>	'caché',
'focusborder'		=>	'bord',
'focussquare'		=>	'case',
'liverate'			=>	'live (mn)',
'autoplay'			=>	'Jeu auto :',
'none'				=>	'aucun',
'game'				=>	'1 fois',
'loop'				=> 	'boucle',
'conf_part_pgn'		=>	'Configuration particulière d’un fichier pgn dans un article SPIP',
'syntaxe'			=>	'Syntaxe',
'url_def_pgn'		=>	'http://fr.wikipedia.org/wiki/Portable_Game_Notation',
'prm_example'		=>	'[pgn</b> <i>prm1</i><b>=</b><i>valeur1</i> <i>prm2</i></i><b>=</b><i>valeur2</i> ...</i><b>]',
'algebric_notation' =>	'Notation algébrique',
'nombre'			=>	'nombre',
'optional_param'	=>	'Liste des paramètres optionnels',
'or'				=>	'ou',
'by_default' 		=>	'par défaut',
'initial'			=>	'initiale',
'in_article'		=>	'Dans le corps d’un article SPIP consacré à la défense Alekhine',
'example'			=>	'Exemple',
'before'			=>	'avant',
'after'				=>	'après'

);