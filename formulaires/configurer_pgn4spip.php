<?php
/**********************************************************************************
 *
 * @copyright copyright (c) 2012 Matt Chesstale 
 * @license GNU General Public License version 3
 *
 **********************************************************************************/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

define('PLUGIN_Form', "configurer_pgn4spip" );

function formulaires_configurer_pgn4spip_traiter() {

	include_spip('inc/cvt_configurer'); include_spip('inc/meta');
	global $optValue; // Current values of options

	if (_request('reinit'))
	{	// User clicks  the "reinit" button 
		effacer_meta('pgn4spip'); // Restores default settings
		return array('message_ok'=>_T('pgn4spip:config_reinit'), 'editable'=>true);
	}
	else // User clicks the "OK" button
	{	// Save current values of options
		cvtconf_formulaires_configurer_enregistre(PLUGIN_Form, func_get_args());
		return array('message_ok'=>_T('config_info_enregistree'), 'editable'=>true);
	}
}